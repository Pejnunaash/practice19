﻿#include <iostream>


class Animal {
public:
   Animal(){}
   void virtual Voice() {
        std::cout << "*Breathing*\n";
    }
};

class Dog : public Animal {
public:
    Dog(){}
   void Voice() override {
        std::cout << "WOOF!\n";
    }
};

class Cat :public Animal {
public:
    Cat(){}
    void Voice() override {
        std::cout << "MIAU!\n";
    }
};

class Cow :public Animal {
public:
    Cow(){}
    void Voice() override {
        std::cout << "MOO!\n";
    }
};

int main()
{
    Animal* AnimalArray[3] = { new Dog(), new Cat(), new Cow() };

    

    for (int i = 0; i < 3; i++) {
        AnimalArray[i]->Voice();
    };
}